import sys  # Use to print size of the List and Tuples
import timeit  # Use to print time to create List and Tuples

# List create with []
primeNumbers = [2, 3, 5, 7, 11, 13, 17]

# Tuple create with () or without ()
perfectSquare = (1, 4, 9, 16, 25, 36)
perfectSquare = 1, 4, 9, 16, 25, 36

# To get data from list and Tuple are the same
for p in primeNumbers:
    print("Prime: ", p)
for n in perfectSquare:
    print("Square: ", n)

# One diffrent between list and Tuples are the methods available for each. List have more methos than Tuples but occupy more memory
listEx = [1, 2, 3, 'a', 'b', 'c', True, 3.14159]
TupleEx = (1, 2, 3, 'a', 'b', 'c', True, 3.14159)

print("List size = ", sys.getsizeof(listEx))
print("Tuple size = ", sys.getsizeof(TupleEx))

# Another diffrent between List and Tuples are the time to made. Tuples are faster than List.
listTime = timeit.timeit(stmt="[1,2,3,4,5]", number=1000000)
tuplesTime = timeit.timeit(stmt="(1,2,3,4,5)", number=1000000)

print("List time = ", listTime)
print("Tuples time = ", tuplesTime)
