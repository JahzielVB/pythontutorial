import math

# Expample 1
print(dir())  # Print actual package


def f():
    pass


print(dir())  # Print actual package, now f function are listed


# Example 2
def volume(r):
    """Returns the volume of a sphere with radius"""  # This is a comment, when help(volume) this comments retuns in help
    return (4.0/3.0) * math.pi * pow(r, 3)


print(volume(2))  # Test


# Example 3
def triangleArea(b, h):
    """Returns the area of a triangule with base b and height h"""  # This is a comment, when help(triangleArea) this comments retuns in help
    return 0.5 * b * h


print(triangleArea(3, 6))


# Example 4
def cm(feet=0, inches=0):  # This function have defaults argumentes
    """Convert a length from feet and inches to centimeters"""
    return ((inches * 2.54) + (feet * 12 * 2.54))


print(cm(feet=5))
print(cm(inches=70))
print(cm(inches=8, feet=5))  # Arguments position dont affect
