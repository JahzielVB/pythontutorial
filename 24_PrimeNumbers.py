import math
import time

# For this lesson I created 3 function version for the same problem, the difrent is the time spend to do it.


def isPrimeV1(n):
    """Return 'True' if 'n' is a prime number. False otherwise. Time: 22.65568995475769 sec"""
    if n == 1:
        return False
    for d in range(2, n):
        if n % d == 0:
            return False
    return True


def isPrimeV2(n):
    """Return 'True' if 'n' is a prime number. False otherwise. Time: 0.13765740394592285 sec"""
    if n == 1:
        return False

    maxDivisor = math.floor(math.sqrt(n))
    for d in range(2, 1 + maxDivisor):
        if n % d == 0:
            return False
    return True


def isPrimeV3(n):
    """Return 'True' if 'n' is a prime number. False otherwise. Time: 0.08162283897399902 sec"""
    if n == 1:
        return False

    if n == 2:
        return True
    if n > 2 and n % 2 == 0:
        return False

    maxDivisor = math.floor(math.sqrt(n))
    for d in range(3, 1 + maxDivisor, 2):
        if n % d == 0:
            return False
    return True


# Test
t0 = time.time()
for n in range(1, 100000):
    isPrimeV3(n)
t1 = time.time()
print("Time required: ", t1 - t0)
