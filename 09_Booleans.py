# Capitalize and Uncapitalize
print(True)  # True and False always be capatilaze, error if is uncapitalize
print(False)  # True and False always be capatilaze, error if is uncapitalize

# Operators
a = 6
b = 4
print(a == b)
print(a != b)
print(a + True)
print(10 * False)

# Types
# 0 is coverted to False, while other number always is converted to True
# String always converted to True, empty string is converted to False
print(bool(28))
print(bool(-45.12))
print(bool(0))
print(bool(" "))
print(bool(""))
