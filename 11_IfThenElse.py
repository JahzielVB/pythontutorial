# -----EXAMPLE 1-----
# Need a something to compare, user input a word and is store the value in inputm
inputm = input("Enter a word: ")

if len(inputm) < 6:  # Check if inputm have less than 6 characters if yes print if not skip the following lines
    print("Word is too short")
    print("Please enter a word a least 6 characteres")


# ------Example 2------
# int convert input to integer, if is not a integer will be a error
inputm = int(input("Enter a number: "))

if inputm % 2 == 0:  # If the condition is false then jump to else
    print("Number is even")
else:
    print("Number is Odd")


# ------Example 3--------
a = int(input("Enter length of side a: "))
b = int(input("Enter length of side b: "))
c = int(input("Enter length of side c: "))

if a != b and b != c and c != a:
    print("Is a scalane triangule")
elif a == b and b == c:
    print("Is a equilateral triangule")
else:
    print("Is a Isosceles triagule")
