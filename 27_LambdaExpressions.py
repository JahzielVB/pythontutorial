# When a function dont have a name it calls Lambda Expressions or anonymous functions.


# Function with name that is f and input value is x
def f(x):
    return 3*x + 1


# Function withot name and input value is x
lambda x: 3*x + 1


def quadraticFun(a, b, c):
    """Return the function f(x) = ax^2 + bx + c"""
    return lambda x: a*x**2 + b*x + c


# Test
print(f(2))
g = quadraticFun(2, 3, -5)
print(g(0))
print(g(1))
