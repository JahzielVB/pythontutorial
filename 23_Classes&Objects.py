import datetime

# Class is a template for creating objects with related data and functions


class User:
    # In class can add a Doctring for help()
    """A member of a social network. For now only storing their name and birthday.
    But soon it will store an uncomfortable amount of user information."""

    # This method calls when instancing a object
    def __init__(self, fullName, birthday):
        self.name = fullName
        self.birthday = birthday

        namePieces = fullName.split(" ")
        self.firstName = namePieces[0]
        self.lastName = namePieces[-1]

    def age(self):
        """Return the age of the user in years."""
        today = datetime.date(2020, 3, 3)
        yyyy = int(self.birthday[0:4])
        mm = int(self.birthday[4:6])
        dd = int(self.birthday[6:8])
        dob = datetime.date(yyyy, mm, dd)
        ageInDays = (today - dob).days
        ageInYears = ageInDays/365
        return ageInYears


user = User("Dave Bowman", "19710315")
print(user.name)
print(user.firstName)
print(user.lastName)
print(user.birthday)
print(user.age())
