# Python 3 comes equipped with a package that simplifies the task of bulding, loading and parsing url.
from urllib import request, parse

# This functions return a respons object that conteint difrents package
resp = request.urlopen("https://es.wikipedia.org")
print(resp.code)
# print(resp.peek())  # That return html


params = {"v": "_P_uiOhZZj4", "t": "32s"}
queryString = parse.urlencode(params)
print(queryString)
url = "https://www.youtube.com/watch" + "?" + queryString
resp2 = request.urlopen(url)
html = resp2.read().decode("utf-8")
print(html[:500])  # That return html
