# Integers
a = 28
print("%s %s" % (a, type(a)))  # print number and type
# Long
b = 21474836471233213211
print("%s %s" % (b, type(b)))  # print number and type
# Float
c = 2.11
print("%s %s" % (c, type(c)))  # print number and type
# Complex
d = 2 + 5.7j
print("%s %s" % (d, type(d)))  # print number and type
print("%s %s" % (d.real, type(d)))  # print real number and type
print("%s %s" % (d.imag, type(d)))  # print imaginary number and type
