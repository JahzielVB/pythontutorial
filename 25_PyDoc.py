# PyDoc module will generate web pages from yout Python documetation. It can launch a web server.
# python -m pydoc.                          -m is to launch as script
# Example: python -m pydoc math             Whit this can see the documetation of math module
# Example: python -m pydoc tuple            Can also see documetation of classes
# python -m pydoc -k Keyword                Can search with a keywords
# Example: python -m pydoc -k ftp           Serch modules with keyword ftp
# python -m pydoc -p port                   This start a server a port selected
