# This is the file who make the test of cicules.py with unittest module

import unittest
from circles import circleArea
from math import pi


class TestCircleArea(unittest.TestCase):
    def testArea(self):
        # Test area when raduis is >= 0
        self.assertAlmostEqual(circleArea(1), pi)
        self.assertAlmostEqual(circleArea(0), 0)
        self.assertAlmostEqual(circleArea(2.1), pi * 2.1**2)

    def testValue(self):
        # Make suer value errors are reaised when necessary
        self.assertRaises(ValueError, circleArea, -2)

    def testTypes(self):
        # Make sure type error are raised when necessary
        self.assertRaises(TypeError, circleArea, 3+5j)
        self.assertRaises(TypeError, circleArea, True)
        self.assertRaises(TypeError, circleArea, "radius")
