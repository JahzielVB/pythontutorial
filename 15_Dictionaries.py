# To create a dictionary, use {"KeyName":value, "KeyName":value}
post = {"userId": 209, "message": "D5 E5 C5 C4 G4", "language": "English",
        "dateTime": "20230215T124231Z", "location": (44.590533, -104.715556)}

# Another way to crate a dictonary is with dict(KeyName="value", KeyName="Value")
post2 = dict(message="SS Cotopaxi", language="English")


# Add value
post2["userID"] = 309
post2["dateTime"] = "20230215T124231Z"

# Access to specefic value
print(post['message'])

# Access to all dictonary
print(post)
