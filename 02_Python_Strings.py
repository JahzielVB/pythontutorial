message = "Meet me tonight"
message2 = 'The clock strikes at midnight'
message3 = 'I\'m looking for someone to share in an adventure' #Need a \ to don't have errors
message4 = 'The phrase "Beam me up, Scotty" was never said on Star Trek' #I can change " to '
message5 = """One of my favorite lines from The Godfather is:\n"I'm going to make him an offer he can't refuse."\nDo you think know who said this?""" #Use triple " for more complex strings.
print(message)
print(message2)
print(message3)
print(message4)
print(message5)