import math  # Import math for example 3
# Example 1
print(dir())  # List available objects
print(dir(__builtins__))  # List specific available objects
print(help(pow))  # Use help to see how use a especific fuction
print(pow(2, 5))  # Test function with help information

# Example 2
print(help(hex))  # See the functionality of hex
print(hex(10))  # Test function with help information

# example 3
print(dir())  # Check if math are avaible
print(help(math.radians))  # Use help to see how use a especific fuction
print(math.radians(180))  # Test function with help information
