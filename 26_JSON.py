# JSON: Javascript Object Notation. It's a small, ligtweight data format. It's identical a Python dictonary.
# json.load(f): Load JSON data from a file (or file-like object).
# json.loads(s): Load JSON data from a string.
# json.dump(j, f): Write JSON object to file (or file-like object).
# json.dumps(j): Output JSON object as string.

import json

# Read a JSON
jsonFile = open(
    "/home/jahzielvb/Documents/Projects/PythonTutorial/PythonTutorial/movie1.txt", "r", encoding="utf-8")
movie = json.load(jsonFile)
jsonFile.close()

print(movie)  # Convert all the data into python rules, also is a dictonary
# As a dictonary, can access to data by key
print(movie["title"])
print(movie["credits"])


# Write a JSON
movie2 = {}
movie2["title"] = "Minitor Report"
movie2["director"] = "Steven Spilberg"
movie2["composer"] = "John Williams"
movie2["actores"] = ["Tom Cruise", "Colin Ferrell",
                     "Samantha Morton", "Max von Sydow"]
movie2["isAwesome"] = True
movie2["budget"] = 102000000
movie2["cinematogrpher"] = "Janusz Kami\u0144ski"

file2 = open("/home/jahzielvb/Documents/Projects/PythonTutorial/PythonTutorial/movie2.txt",
             "w", encoding="utf-8")
json.dump(movie2, file2, ensure_ascii=False)
file2.close()
