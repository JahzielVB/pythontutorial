# To create and fill a list without comprehension
squares = []
for i in range(1, 101):
    squares.append(i**2)

print(squares)


# To create and fill a list with comprehension
squares2 = [i**2 for i in range(1, 101)]
print(squares2)

remainders = [x**2 % 5 for x in range(1, 101)]
print(remainders)


A = [1, 3, 5, 7]
B = [2, 4, 6, 8]

cartesianProduct = [(a, b) for a in A for b in B]
print(cartesianProduct)
