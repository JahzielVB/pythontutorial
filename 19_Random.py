import random

# To display randoms numbers
for i in range(10):
    print(random.random())

# To get a random numbers in a range
for i in range(10):
    print(random.uniform(3, 9))

# To get normal distribution aka "Bell curve"
for i in range(10):
    print(random.normalvariate(0, 9))

# To get integer in range
for i in range(10):
    print(random.randint(1, 6))

# Get a random element from a list
outcomes = ['Rock', 'Paper', 'Scissors']
for i in range(10):
    print(random.choice(outcomes))
