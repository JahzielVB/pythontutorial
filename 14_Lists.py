# Two ways to create a list
# example = list()
# example = []

primes = [2, 3, 5, 7, 11, 13]
# append add values to the end of the list
primes.append(17)  # Add new value
primes.append(19)  # Add new value
print(primes)  # Test new values in the list

# Access to a value with index
print(primes[2])  # Select especific value with index
print(primes[-1])  # When is index negative start from the end

# Access to a value with slicing
print(primes[2:5])  # With this have a sub index


# List can contein difrents types of data even other lists
example = [128, True, "Alpha", 3.14156, [64, False]]
