a = 2  # int
b = 6.0  # float
c = 12+0j  # Complex number

# Rule: Widen numbers so they're the same type

# Adition
print(a + b)  # int + float = float

# Subtraction
print(b - a)  # float - int = float

# Multiplication
print(a * 7)  # int * int = int

# Division
print(c / b)  # Complex / float = complex
print(16 / 5)  # Result int
print(16.0 / 5.0)  # Result float
print(16 % 5)  # Result remainder
print(16 // 5)  # Result Quotient
