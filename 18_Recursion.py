from functools import lru_cache

# The best example of recursion are fibonacci sequence
# If run this method without lru_cache for lots of iterations, the compute are imposible
@lru_cache(maxsize=1000)
def fibonacci(n):
    if n == 1:
        return 1
    elif n == 2:
        return 1
    elif n > 2:
        return fibonacci(n-1) + fibonacci(n-2)


for n in range(1, 501):
    print(n, ":", fibonacci(n))
