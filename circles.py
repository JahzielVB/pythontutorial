# This is main file that contain the function to be tested

from math import pi


def circleArea(r):
    if type(r) not in [int, float]:
        raise TypeError("The readius must be a non-negative real number.")

    if r < 0:
        raise TypeError("The radius canont be negative.")
    return pi*(r**2)
