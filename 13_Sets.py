example = set()  # Create a set, name ir example
# Can add difrents types of data in one set
example.add(42)
example.add(False)
example.add(3.14159)
example.add("Thorium")

# When print it, see in difrent order. For sets the order does not matter
print(example)

# If add a new data that are in set, Set dont coteins duplicate elements
example.add(42)
print(example)

print(len(example))  # Display number of elements in set
example.remove(42)  # Remove specific element from set

example2 = set([28, True, 2.7182, "Helium"])  # Create a set with data inside
example2.clear()  # Delete all data

# Union and Intersection for two sets
odds = set([1, 3, 5, 7, 9])
evens = set([2, 4, 6, 8, 10])
primes = set([2, 3, 5, 7])
composites = set([4, 6, 8, 9, 10])

print(odds.union(evens))
print(evens.union(odds))
print(odds.intersection(primes))
print(primes.intersection(evens))
# Odds, Evens, primes and composites set unchange

# Can check if a data are inside in one set
print(6 in primes)
print(10 in evens)
print(4 in composites)
