import logging  # Import for have logger
import math  # Import for test example

# Create and configure logger
logFormat = "%(levelname)s %(asctime)s - %(message)s"
logging.basicConfig(filename="/home/jahzielvb/Documents/Projects/PythonTutorial/PythonTutorial/logMessage.log",
                    level=logging.DEBUG, format=logFormat, filemode='w')
logger = logging.getLogger()

# Test messages
logger.debug("This is a debug message")
logger.info("This is a info message")
logger.warning("This is a warning message")
logger.error("This is a error message")
logger.critical("This is a critical message")


# Test in a real state
def quadraticForm(a, b, c):
    """Return the solution to the equation ax^2 + bx + c = 0"""
    logger.info("quadraticForm({0}, {1}, {2})".format(a, b, c))

    # Compute the discriminant
    logger.debug("# Compute the discriminant")
    disc = b**2 - 4*a*c

    # Compute the two roots
    logger.debug("# Compute the two roots")
    root1 = (-b + math.sqrt(disc)) / (2*a)
    root2 = (-b - math.sqrt(disc)) / (2*a)

    # Return the roots
    logger.debug("# Return the roots")
    return (root1, root2)


roots = quadraticForm(1, 0, -4)
print(roots)

# In terminal just print the result, but in the logger print all the process
