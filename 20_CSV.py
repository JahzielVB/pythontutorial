import csv
from datetime import datetime

path = "/home/jahzielvb/Documents/Projects/PythonTutorial/PythonTutorial/google_stock_data.csv"

# --Store data from CSV without module csv--
# With this, the result is just one string and have line character at the end
lines = [line for line in open(path)]
print(lines[0])
print(lines[1])


# To separete and delete line character use.
lines2 = [line.strip().split(',') for line in open(path)]
print(lines[0])
print(lines[1])
# But the date are still string and special case with data that conteins ',' have problems


# --Store data from CSV with module csv--
file = open(path, newline='')
reader = csv.reader(file)
hearder = next(reader)

data = []
for row in reader:
    date = datetime.strptime(row[0], '%m/%d/%Y')
    openPrice = float(row[1])
    high = float(row[2])
    low = float(row[3])
    close = float(row[4])
    volume = int(row[5])
    adjClose = float(row[6])

    data.append([date, openPrice, high, low, close, volume, adjClose])

print(data[0])
