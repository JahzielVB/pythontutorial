# There are two types of files:
# Text Files are generaly humand readable data, like plain text, XML, JSOor even source code.
# Binary files are used for sorting compiled code, like appdata, media files, images, audio, video.

# To open a text file in python is call the open function, but if somthing happend and you can't close this can damage you memory
# ---------------f = open("guidoBio.txt")
# ---------------text = f.read()
# ---------------f.close()
# ---------------print(text)

# To open a text file without danger and also 33% shorter
with open("guidoBio.txt") as fobj:
    bio = fobj.read()

print(bio)

# To write in text file
oceans = ["Pacific", "Atlantic", "Indian", "Southern", "Arctic"]

with open("oceans.txt", "w") as f:
    for oceans in oceans:
        print(oceans, file=f)

with open("oceans.txt", "a") as f:
    print(23*"=", file=f)
    print("There are the 5 oceans.", file=f)
