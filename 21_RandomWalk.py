# Supuse yo are in a grid that represent a city, you decide to walk at each intersection you decide you next direction randomly.
# When you finish you check how far away you are form home.
# If is more than 4 block away you take transport.
# What is the  longest random walk you can take so that on avarage you will end up 4 block sor fewer from home?

import random


def randomWalk(n):
    """Return coordinates after 'n' block random walk."""
    x, y = 0, 0
    for _ in range(n):
        (dx, dy) = random.choice([(0, 1), (0, -1), (1, 0), (-1, 0)])
        x += dx
        y += dy
    return (x, y)


numbersOfWalk = 20000

for walkLength in range(1, 31):
    noTransport = 0
    for i in range(numbersOfWalk):
        (x, y) = randomWalk(walkLength)
        distance = abs(x) + abs(y)
        if distance <= 4:
            noTransport += 1
    noTransportPercentage = float(noTransport) / numbersOfWalk
    print("Walk size = ", walkLength,
          " / % of transport = ", 100*noTransportPercentage)
