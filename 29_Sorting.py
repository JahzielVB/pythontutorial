# When sort method calls, the list are modify. Only can sorf list, tuple no.
earthMetals = ["Beryllium", "Magnesium",
               "Calcium", "Strontium", "Barium", "Radium"]
# Sort alphabetically ascending
# earthMetals.sort()
# print(earthMetals)
# Sort alphabetically reverse
# earthMetals.sort(reverse=True)
# print(earthMetals)


# To sort a tuble or make a copy but sorted
sortedEarthMetals = sorted(earthMetals)
# Original list
print(earthMetals)
# List sorted
print(sortedEarthMetals)

# Sort a tuple result a list
data = (7, 2, 5, 6, 1, 3, 9, 10, 4, 8)
print(sorted(data))
