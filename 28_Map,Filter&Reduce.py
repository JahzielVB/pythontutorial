import math

from functools import reduce

# ==================== Map function =======================


def area(r):
    """Area of a circle with radus 'r'"""
    return math.pi * (r**2)


radii = [2, 5, 7.1, 0.3, 10]
# Method 1: Direct method
areas = []
for r in radii:
    a = area(r)
    areas.append(a)

# Method 2: Use 'mao' function
a = list(map(area, radii))


# Test
print(areas)
print(a)


# ==================== Filter function =======================
countries = ["", "Argentina", "", "Brazil", "", "", "Chile",
             "Colombia", "", "", "", "Ecuador", "", "Venezuela"]
print(list(filter(None, countries)))  # Filter remove missing data


# ==================== Reduce function =======================
# "Use functools.reduce() if you really need it; howaver, 99% of the time an explicit for loop is more readable." -Guido Van Rossum
data = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
multiplier = lambda x, y: x*y
print(reduce(multiplier, data))

product = 1
for x in data:
    product = product * x

print(product)


